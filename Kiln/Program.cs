﻿using Culina.Vendors.SeaChange.Controllers;
using Kiln.Properties;
using Mono.Options;
using System;
using SwankParser = Culina.Vendors.Swank.Controllers.Parser;

namespace Kiln
{
    class Program
    {
        static string dirIn;
        static string dirOut;

        static void Main(string[] args)
        {
            //LockUserSettings();
            //ProcessFiles();
            ProcessArguments(args);
        }

        static void DispatchAction(string action, string[] args)
        {
            Dispatcher.Action = action;
            Dispatcher.Options = args;

            Dispatcher.Go();
        }

        static void ProcessArguments(string[] args)
        {
            var p = new OptionSet()
            {
                {
                    "a|action =", 
                    "The action to dispatch.", 
                    v => DispatchAction(v, args)
                }
            };

            try
            {
                p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine("Errors: ");
                Console.WriteLine(e.Message);
            }
        }

        static void ProcessFiles()
        {
            string dirIn = Settings.Default.InputDirectory;
            string dirOut = Settings.Default.OutputDirectory;

            var swank = new SwankParser(dirIn, "xml");
            var films = swank.ReadFiles();

            var scfw = new FromSwank();
            scfw.BatchConvert(films, dirOut);

            var rfs = new Culina.Vendors.Razuna.Controllers.FromSwank();
            var rfilms = rfs.MakeFilmList(films);
            rfs.ToCsv(rfilms, dirOut);
        }

        private static void LockUserSettings()
        {
            if (Settings.Default.UpgradeRequired)
            {
                Settings.Default.Upgrade();
                Settings.Default.UpgradeRequired = false;
                Settings.Default.Save();
            }
        }

        private static void ShowHelp()
        {
            Console.WriteLine("THis is what help would be");
        }
    }
}
