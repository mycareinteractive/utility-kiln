﻿


using Mono.Options;
using System;

namespace Kiln
{
    public class Dispatcher
    {
        public static string Action { get; set; }
        public static string[] Options { get; set; }

        public static void Parse(string[] opt)
        {
            Console.WriteLine("We are parsing");
        }

        public static void Go()
        {
            var p = new OptionSet();

            switch (Action)
            {
                case "parse":

                    p.Add(
                        "it|input_type =",
                        "This sets the input parse type. Acceptable values are: swank, adi",
                        v => Controllers.Parse.InputType = v
                    );

                    p.Add(
                        "ot|output_type =",
                        "This sets the output parse type. Acceptable values are: razuna, adi",
                        v => Controllers.Parse.OutputType = v
                    );

                    p.Add(
                        "i|in =",
                        "This sets the input directory.",
                        v => Controllers.Parse.Input = v
                    );

                    p.Add(
                        "o|out =",
                        "This sets the output directory.",
                        v => Controllers.Parse.Output = v
                    );

                    break;
                default:
                    break;
            }

            try
            {
                p.Parse(Options);
            }
            catch (OptionException e)
            {
                Console.WriteLine("Errors: ");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("InputType: " + Controllers.Parse.InputType);
            Controllers.Parse.Run(Options);

        }

        public Dispatcher(string[] args)
        {

        }
    }
}
