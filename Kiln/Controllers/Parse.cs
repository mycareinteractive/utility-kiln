﻿
using Culina.Vendors.SeaChange.Controllers;
using System;
using SwankParser = Culina.Vendors.Swank.Controllers.Parser;

namespace Kiln.Controllers
{

    public class Parse
    {
        public static string InputType { get; set; }
        public static string OutputType { get; set; }
        public static string Input { get; set; }
        public static string Output { get; set; }

        private static Boolean ValidateOptions()
        {
            var process = true;

            if (InputType == null)
            {
                Console.WriteLine("FAILED: Input Type is required");
                process = false;
            }

            if (OutputType == null)
            {
                Console.WriteLine("FAILED: Output Type is required");
                process = false;
            }

            if (Input == null)
            {
                Console.WriteLine("FAILED: Input is required");
                process = false;
            }

            if (Output == null)
            {
                Console.WriteLine("FAILED: Output is required");
                process = false;
            }

            return process;
        }

        public static void Run(string[] args)
        {

            Boolean valid = ValidateOptions();

            if (valid == false)
                return;

            string dirIn = Input;
            string dirOut = Output;

            var swank = new SwankParser(dirIn, "xml");
            var films = swank.ReadFiles();

            switch (InputType)
            {
                case "swank":
                    switch (OutputType)
                    {
                        case "adi":
                            var scfw = new FromSwank();
                            scfw.BatchConvert(films, dirOut);
                            break;
                        case "razuna":
                            var rfs = new Culina.Vendors.Razuna.Controllers.FromSwank();
                            var rfilms = rfs.MakeFilmList(films);
                            rfs.ToCsv(rfilms, dirOut);
                            break;
                        case "wired.md":
                            break;
                    }
                    break;
                default:
                    break;
            }

        }
    }
}
