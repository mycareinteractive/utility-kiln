﻿using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
//using System.Json;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Culina.Tools
{
    public class Razuna
    {
        // string apiKey = "0B0B1C0DBECC4230BD8E246C6D9DB409";
        string apiKey = "4C813F44256C43F4B66FD6526C46FDF9";
        //string folderId = "5EC65F8B086945D69958489877948FEF";
        string folderId = "08AABE45D0784438A2362CF714901194";
        //string apiBaseUrl = "http://homer.aceso.com:8080/";
        string apiBaseUrl = "http://picon.local:8080/";
        //string apiBaseUrl = "";
        string apiUrl = "razuna/raz1/dam/index.cfm";
        //string apiUrl = "http://homer.aceso.com:8080/razuna/raz1/dam/index.cfm";
        string testVideoFile = @"C:\Users\gpunx\Clients\Aceso\projects\Kiln\_video\StartMenu_Win8.mp4";
        string fa = "c.apiupload";

        async Task uploader()
        {
            var boundary = DateTime.Now.ToString("yyyyMMddHHmmssffff");

            using (var client = new HttpClient())
            using (var content = new MultipartFormDataContent(boundary))
            {
                // Make sure to change API address
                client.BaseAddress = new Uri(apiBaseUrl);

                content.Headers.Remove("Content-Type");
                content.Headers.Add("Content-Type", "multipart/form-data; boundary=" + boundary);

                var values = new[]
                {
                    new KeyValuePair<string, string>("api_key", apiKey),
                    new KeyValuePair<string, string>("fa", fa),
                    new KeyValuePair<string, string>("destfolderid", folderId),
                    new KeyValuePair<string, string>("metadata", "1"),
                    new KeyValuePair<string, string>("meta_lang_id_r", "1"),
                    new KeyValuePair<string, string>("meta_aceso.genre", "funkyness"),
                    new KeyValuePair<string, string>("meta_description", "this is a sample upload")
                };

                foreach (var keyValuePair in values)
                {
                    content.Add(new StringContent(keyValuePair.Value), String.Format("\"{0}\"", keyValuePair.Key));
                }

                // Add first file content 
                var video = new StreamContent(File.OpenRead(testVideoFile));
                //var video = new ByteArrayContent(File.ReadAllBytes(testVideoFile));
                video.Headers.ContentType = new MediaTypeHeaderValue("video/mp4");
                video.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "\"filedata\"",
                    FileName = "\"StartMenu_Win8.mp4\""
                };

                content.Add(video);

                // Make a call to Web API
                var request = await client.PostAsync(apiUrl, content);
                var astring = await request.Content.ReadAsStringAsync();

                Console.WriteLine("STATUS: " + request.StatusCode);
                Console.WriteLine("CONTENT: " + astring);
            }
        }

        private void parseResponse(string resp = null)
        {
            if (resp == null)
            {
                resp = @"<?xml version='1.0' encoding='UTF-8'?>
                            <Response>
                                <responsecode>0</responsecode>
                                <message>success</message>
                                <assetid>53D380BE59714B118C176A28E8367824</assetid>
                                <filetype>vid</filetype>
                                <comingfrom><![CDATA[]]></comingfrom>
                                <renamefilebody><![CDATA[]]></renamefilebody>
                            </Response>";
            }

            var parser = new HtmlParser();
            var dom = parser.Parse(resp);
            var assetids = dom.QuerySelectorAll("assetid");
            var aid = assetids.First().TextContent;

            Console.WriteLine("AssetID: " + aid);
        }

        public Razuna()
        {
            // video/mpeg
            uploader();
            // parseResponse();
        }
    }
}
