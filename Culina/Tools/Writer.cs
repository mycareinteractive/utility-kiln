﻿using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Culina.Tools
{
    public class Writer
    {
        public StringBuilder Formatter(string xDoc)
        {
            // format xml string
            var stringBuilder = new StringBuilder();
            var element = XElement.Parse(xDoc);
            var settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = false;
            settings.Indent = true;
            settings.NewLineOnAttributes = false;

            XmlWriter xWriter;

            using (xWriter = XmlWriter.Create(stringBuilder, settings))
            {
                element.Save(xWriter);
            }

            return stringBuilder;
        }

        public Writer()
        {

        }
    }
}
