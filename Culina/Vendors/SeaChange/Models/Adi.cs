﻿


namespace Culina.Vendors.SeaChange.Models
{

    /// <remarks/>
    //[System.Xml.Serialization.XmlElementAttribute("ADI")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ADI
    {

        private ADIMetadata metadataField;

        private ADIAsset assetField;

        /// <remarks/>
        public ADIMetadata Metadata
        {
            get
            {
                return this.metadataField;
            }
            set
            {
                this.metadataField = value;
            }
        }

        /// <remarks/>
        public ADIAsset Asset
        {
            get
            {
                return this.assetField;
            }
            set
            {
                this.assetField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIMetadata
    {

        private ADIMetadataAMS aMSField;

        private ADIMetadataApp_Data[] app_DataField;

        /// <remarks/>
        public ADIMetadataAMS AMS
        {
            get
            {
                return this.aMSField;
            }
            set
            {
                this.aMSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("App_Data")]
        public ADIMetadataApp_Data[] App_Data
        {
            get
            {
                return this.app_DataField;
            }
            set
            {
                this.app_DataField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIMetadataAMS
    {

        private string asset_NameField;

        private string providerField;

        private string productField;

        private byte version_MajorField;

        private byte version_MinorField;

        private string descriptionField;

        private int creation_DateField;

        private byte provider_IDField;

        private string asset_IDField;

        private string asset_ClassField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Name
        {
            get
            {
                return this.asset_NameField;
            }
            set
            {
                this.asset_NameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Provider
        {
            get
            {
                return this.providerField;
            }
            set
            {
                this.providerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Major
        {
            get
            {
                return this.version_MajorField;
            }
            set
            {
                this.version_MajorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Minor
        {
            get
            {
                return this.version_MinorField;
            }
            set
            {
                this.version_MinorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "int")]
        public int Creation_Date
        {
            get
            {
                return this.creation_DateField;
            }
            set
            {
                this.creation_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Provider_ID
        {
            get
            {
                return this.provider_IDField;
            }
            set
            {
                this.provider_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_ID
        {
            get
            {
                return this.asset_IDField;
            }
            set
            {
                this.asset_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Class
        {
            get
            {
                return this.asset_ClassField;
            }
            set
            {
                this.asset_ClassField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIMetadataApp_Data
    {

        private string appField;

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string App
        {
            get
            {
                return this.appField;
            }
            set
            {
                this.appField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAsset
    {

        private ADIAssetMetadata metadataField;

        private ADIAssetAsset[] assetField;

        /// <remarks/>
        public ADIAssetMetadata Metadata
        {
            get
            {
                return this.metadataField;
            }
            set
            {
                this.metadataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Asset")]
        public ADIAssetAsset[] Asset
        {
            get
            {
                return this.assetField;
            }
            set
            {
                this.assetField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetMetadata
    {

        private ADIAssetMetadataAMS aMSField;

        private ADIAssetMetadataApp_Data[] app_DataField;

        /// <remarks/>
        public ADIAssetMetadataAMS AMS
        {
            get
            {
                return this.aMSField;
            }
            set
            {
                this.aMSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("App_Data")]
        public ADIAssetMetadataApp_Data[] App_Data
        {
            get
            {
                return this.app_DataField;
            }
            set
            {
                this.app_DataField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetMetadataAMS
    {

        private string asset_NameField;

        private string providerField;

        private string productField;

        private byte version_MajorField;

        private byte version_MinorField;

        private string descriptionField;

        private int creation_DateField;

        private byte provider_IDField;

        private int asset_IDField;

        private string asset_ClassField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Name
        {
            get
            {
                return this.asset_NameField;
            }
            set
            {
                this.asset_NameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Provider
        {
            get
            {
                return this.providerField;
            }
            set
            {
                this.providerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Major
        {
            get
            {
                return this.version_MajorField;
            }
            set
            {
                this.version_MajorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Minor
        {
            get
            {
                return this.version_MinorField;
            }
            set
            {
                this.version_MinorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "int")]
        public int Creation_Date
        {
            get
            {
                return this.creation_DateField;
            }
            set
            {
                this.creation_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Provider_ID
        {
            get
            {
                return this.provider_IDField;
            }
            set
            {
                this.provider_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Asset_ID
        {
            get
            {
                return this.asset_IDField;
            }
            set
            {
                this.asset_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Class
        {
            get
            {
                return this.asset_ClassField;
            }
            set
            {
                this.asset_ClassField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetMetadataApp_Data
    {

        private string appField;

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string App
        {
            get
            {
                return this.appField;
            }
            set
            {
                this.appField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetAsset
    {

        private ADIAssetAssetMetadata metadataField;

        /// <remarks/>
        public ADIAssetAssetMetadata Metadata
        {
            get
            {
                return this.metadataField;
            }
            set
            {
                this.metadataField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetAssetMetadata
    {

        private ADIAssetAssetMetadataAMS aMSField;

        private ADIAssetAssetMetadataApp_Data[] app_DataField;

        private ADIAssetAssetMetadataContent contentField;

        /// <remarks/>
        public ADIAssetAssetMetadataAMS AMS
        {
            get
            {
                return this.aMSField;
            }
            set
            {
                this.aMSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("App_Data")]
        public ADIAssetAssetMetadataApp_Data[] App_Data
        {
            get
            {
                return this.app_DataField;
            }
            set
            {
                this.app_DataField = value;
            }
        }

        /// <remarks/>
        public ADIAssetAssetMetadataContent Content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetAssetMetadataAMS
    {

        private string asset_NameField;

        private string providerField;

        private string productField;

        private byte version_MajorField;

        private byte version_MinorField;

        private string descriptionField;

        private int creation_DateField;

        private byte provider_IDField;

        private string asset_IDField;

        private string asset_ClassField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Name
        {
            get
            {
                return this.asset_NameField;
            }
            set
            {
                this.asset_NameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Provider
        {
            get
            {
                return this.providerField;
            }
            set
            {
                this.providerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Product
        {
            get
            {
                return this.productField;
            }
            set
            {
                this.productField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Major
        {
            get
            {
                return this.version_MajorField;
            }
            set
            {
                this.version_MajorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Version_Minor
        {
            get
            {
                return this.version_MinorField;
            }
            set
            {
                this.version_MinorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "int")]
        public int Creation_Date
        {
            get
            {
                return this.creation_DateField;
            }
            set
            {
                this.creation_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Provider_ID
        {
            get
            {
                return this.provider_IDField;
            }
            set
            {
                this.provider_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_ID
        {
            get
            {
                return this.asset_IDField;
            }
            set
            {
                this.asset_IDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Asset_Class
        {
            get
            {
                return this.asset_ClassField;
            }
            set
            {
                this.asset_ClassField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetAssetMetadataApp_Data
    {

        private string appField;

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string App
        {
            get
            {
                return this.appField;
            }
            set
            {
                this.appField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ADIAssetAssetMetadataContent
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}
