﻿using Culina.Vendors.SeaChange.Models;
using Culina.Vendors.Swank.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Culina.Vendors.SeaChange.Controllers
{
    public class FromSwank : Tools.Writer
    {
        private string placeHolder = "### [ PLACEHOLDER: Where's This Data Coming From? ] ###";

        public void Init(Film film)
        {


            var packageMetadata = new List<List<KeyValuePair<string, string>>>();

            var titleAms = new List<KeyValuePair<string, string>>();
            var titleMetadata = new List<List<KeyValuePair<string, string>>>();

            var movieAms = new List<KeyValuePair<string, string>>();
            var movieMetadata = new List<List<KeyValuePair<string, string>>>();
            var movieContent = new List<KeyValuePair<string, string>>();

            var posterAms = new List<KeyValuePair<string, string>>();
            var posterMetadata = new List<List<KeyValuePair<string, string>>>();
            var posterContent = new List<KeyValuePair<string, string>>();

            var previewAms = new List<KeyValuePair<string, string>>();
            var previewMetadata = new List<List<KeyValuePair<string, string>>>();
            var previewContent = new List<KeyValuePair<string, string>>();
        }

        public StringBuilder ToAdiXml(Film film)
        {
            /**
             * ADI root
             **/
            ADI adi = new ADI();
            adi.Metadata = new ADIMetadata();
            adi.Asset = new ADIAsset();
            adi.Asset.Metadata = new ADIAssetMetadata();

            adi.Metadata.AMS = new ADIMetadataAMS()
            {
                Asset_Name = film.Title,
                Provider = "Swank",
                Version_Major = 1,
                Version_Minor = 0,
                Description = film.Title + " package",
                Creation_Date = Convert.ToInt32(film.Start),
                Provider_ID = 002,
                Asset_ID = film.ID,
                Asset_Class = "package"
            };
            adi.Metadata.App_Data = new ADIMetadataApp_Data[]
            {
                new ADIMetadataApp_Data()
                {
                    App = "MOD",
                    Name = "Metadata_Spec_Version",
                    Value = "CableLabsVOD1.1"
                },
                new ADIMetadataApp_Data()
                {
                    App = "MOD",
                    Name = "Metadata_Spec_Version",
                    Value = "CableLabsVOD1.1"
                }
            };


            /**
             * Title
             **/

            adi.Asset.Metadata.AMS = new ADIAssetMetadataAMS()
            {
                Asset_Name = film.Title + " title",
                Provider = "Swank",
                Version_Major = 1,
                Version_Minor = 0,
                Description = film.Title + " title",
                Creation_Date = Convert.ToInt32(film.Start),
                Provider_ID = 002,
                Asset_ID = Convert.ToInt32(film.ID),
                Asset_Class = "title"
            };

            adi.Asset.Metadata.App_Data = new ADIAssetMetadataApp_Data[]
            {
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Title_Brief",
                    Value = film.Short_Title
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Title",
                    Value = film.Title
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Summary_Long",
                    Value = film.DetailedDescription
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Summary_Medium",
                    Value = film.DetailedDescription
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Summary_Short",
                    Value = film.DetailedDescription
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Rating",
                    Value = ""
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Closed_Captioning",
                    Value = "false"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Run_Time",
                    Value = Convert.ToString(film.Runtime)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Display_Run_Time",
                    Value = Convert.ToString(film.Runtime)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Year",
                    Value = Convert.ToString(film.Year)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Studio",
                    Value = "Swank"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Category",
                    Value = film.Genres.First()
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Provider_Asset_ID",
                    Value = Convert.ToString(film.ID)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Season_Premier",
                    Value = "N"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Season_Finale",
                    Value = "N"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Genre",
                    Value = String.Join(", ", film.Genres)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Licensing_Window_Start",
                    Value = Convert.ToString(film.Start)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Licensing_Window_End",
                    Value = Convert.ToString(film.End)
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Preview_Period",
                    Value = "10"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Maximum_Viewing_Length",
                    Value = "99:00:00"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Provider_QA_Contact",
                    Value = ""
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Suggested_Price",
                    Value = "0.00"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Distributor_Name",
                    Value = "Aceso"
                },
                new ADIAssetMetadataApp_Data{
                    App = "MOD",
                    Name = "Studio_Name",
                    Value = "Swank"
                }
            };


            /**
             * MOVIE
             **/

            var movie = new ADIAssetAssetMetadata();


            movie.AMS = new ADIAssetAssetMetadataAMS()
            {
                Asset_Name = film.Title + " movie",
                Provider = "Swank",
                Version_Major = 1,
                Version_Minor = 0,
                Description = film.Title + " title",
                Creation_Date = Convert.ToInt32(film.Start),
                Provider_ID = 002,
                Asset_ID = film.ID + "m",
                Asset_Class = "movie"
            };

            movie.App_Data = new ADIAssetAssetMetadataApp_Data[]
            {
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Type",
                    Value = "movie"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_FileSize",
                    Value = ""
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_CheckSum",
                    Value = ""
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Encryption",
                    Value = "N"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Encryption",
                    Value = "N"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Copy_Protection",
                    Value = "N"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "HDContent",
                    Value = "N"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Viewing_Can_Be_Resumed",
                    Value = "N"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_Format",
                    Value = "MPEG2"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_Type",
                    Value = "MP2TS"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Screen_Format",
                    Value = "Standard"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Audio_Type",
                    Value = "s"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Languages",
                    Value = "film.Language"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Subtitle_Languages",
                    Value = "e"
                }
            };

            movie.Content = new ADIAssetAssetMetadataContent()
            {
                Value = film.Mediafile
            };


            /**
             * Poster
             **/

            var poster = new ADIAssetAssetMetadata();

            poster.AMS = new ADIAssetAssetMetadataAMS()
            {
                Asset_Name = film.Title + " poster",
                Provider = "Swank",
                Version_Major = 1,
                Version_Minor = 0,
                Description = film.Title + " title",
                Creation_Date = Convert.ToInt32(film.Start),
                Provider_ID = 002,
                Asset_ID = film.ID + "p",
                Asset_Class = "poster"
            };

            poster.App_Data = new ADIAssetAssetMetadataApp_Data[]
            {
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Type",
                    Value = "poster"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_FileSize",
                    Value = ""
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_CheckSum",
                    Value = ""
                }
            };

            poster.Content = new ADIAssetAssetMetadataContent()
            {
                Value = film.Cover
            };


            /**
             * Preview
             **/

            var preview = new ADIAssetAssetMetadata();

            preview.AMS = new ADIAssetAssetMetadataAMS()
            {
                Asset_Name = film.Title + " preview",
                Provider = "Swank",
                Version_Major = 1,
                Version_Minor = 0,
                Description = film.Title + " preview",
                Creation_Date = Convert.ToInt32(film.Start),
                Provider_ID = 002,
                Asset_ID = film.ID + "pr",
                Asset_Class = "preview"
            };

            preview.App_Data = new ADIAssetAssetMetadataApp_Data[]
            {
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Type",
                    Value = "preview"
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_FileSize",
                    Value = ""
                },
                new ADIAssetAssetMetadataApp_Data
                {
                    App = "MOD",
                    Name = "Content_CheckSum",
                    Value = ""
                }
            };

            preview.Content = new ADIAssetAssetMetadataContent()
            {
                Value = film.Mediafile
            };


            adi.Asset.Asset = new ADIAssetAsset[]
            {
                new ADIAssetAsset()
                {
                    Metadata = movie
                }, 
                new ADIAssetAsset()
                {
                    Metadata = poster
                }, 
                new ADIAssetAsset()
                {
                    Metadata = preview
                }
            };

            XmlSerializer xadi = null;
            StringBuilder stringBuilder;

            xadi = new XmlSerializer(adi.GetType());


            using (StringWriter writer = new StringWriter())
            {
                xadi.Serialize(writer, adi);
                stringBuilder = Formatter(writer.ToString());
            }

            Console.WriteLine();
            Console.WriteLine("AD XML DOC:");
            Console.WriteLine(stringBuilder.ToString());

            return stringBuilder;
        }

        public void BatchConvert(List<Film> films, string dirOut = null)
        {
            foreach (var film in films)
            {
                var sXml = ToAdiXml(film);
                if (dirOut != null)
                {
                    using (StreamWriter outputFile = new StreamWriter(dirOut + @"\" + film.ID + ".xml"))
                    {
                        outputFile.WriteLine(sXml);
                    }

                }
            }
        }

        public FromSwank()
        {
        }
    }
}
