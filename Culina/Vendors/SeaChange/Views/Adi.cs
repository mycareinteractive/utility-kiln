﻿using System.Collections.Generic;
using System.Xml;

namespace Culina.Vendors.SeaChange.Views
{
    public class Adi
    {
        public XmlDocument doc;
        public XmlElement adi;
        public XmlElement asset;
        public XmlElement metadata;
        public XmlElement ams;
        public XmlElement content;

        public XmlElement CreateXmlNode(string name)
        {
            XmlElement node = doc.CreateElement(name);
            return node;
        }

        public XmlElement CreateAssetXmlNode()
        {
            XmlElement asset = doc.CreateElement("Asset");
            return asset;
        }

        public XmlElement CreateMetadataXmlNode()
        {
            XmlElement metadata = doc.CreateElement("Metadata");
            return metadata;
        }

        public XmlElement CreateAmsXmlNode(List<KeyValuePair<string, string>> attrs)
        {
            XmlElement ams = doc.CreateElement("AMS");
            foreach (var el in attrs)
            {
                ams.SetAttribute(el.Key, el.Value);
            }
            return ams;
        }

        public XmlElement CreateContentXmlNode(List<KeyValuePair<string, string>> attrs)
        {
            XmlElement content = doc.CreateElement("Content");
            foreach (var el in attrs)
            {
                content.SetAttribute(el.Key, el.Value);
            }
            return content;
        }

        public XmlElement CreateAppDataXmlNode(List<KeyValuePair<string, string>> attrs)
        {
            XmlElement appdata = doc.CreateElement("App_Data");
            foreach (var el in attrs)
            {
                appdata.SetAttribute(el.Key, el.Value);
            }
            return appdata;
        }

        public Adi(XmlDocument doc = null)
        {
            this.doc = doc ?? new XmlDocument();
        }
    }
}
