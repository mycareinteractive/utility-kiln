﻿using AngleSharp.Dom.Xml;
using Culina.Vendors.Swank.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Culina.Vendors.Swank.Controllers
{
    public class Parser
    {
        private Film Film;

        public List<string> Files;
        public IXmlDocument XmlDoc;
        public List<Film> Films;

        public List<string> GetFiles(string dir, string fileExtension, List<string> files = null)
        {
            var ufiles = files ?? new List<string>();

            try
            {
                foreach (string d in Directory.GetDirectories(dir))
                {
                    foreach (string f in Directory.GetFiles(d, "*." + fileExtension))
                    {
                        ufiles.Add(f);
                    }
                    GetFiles(d, fileExtension, ufiles);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }

            return ufiles;
        }

        public List<Film> ReadFiles()
        {
            foreach (string f in Files)
            {
                Film = new Film();
                XmlSerializer serial = new XmlSerializer(typeof(Film));
                using (XmlReader reader = XmlReader.Create(f))
                {
                    try
                    {
                        Film = (Film)serial.Deserialize(reader);
                        Films.Add(Film);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(Path.GetFileName(f) + " may not be a valid Swank Film XML file.");
                    }
                }

            }

            return Films;
        }

        public Parser(string directory, string fileExtension)
        {
            Films = new List<Film>();
            Files = GetFiles(directory, fileExtension);
        }
    }
}
