﻿


namespace Culina.Vendors.Swank.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Film
    {

        private string titleField;

        private string short_TitleField;

        private string ratingField;

        private string[] genresField;

        private byte runtimeField;

        private uint startField;

        private uint endField;

        private string languageField;

        private string coverField;

        private string[] actorsField;

        private string[] directorsField;

        private string mediafileField;

        private ushort yearField;

        private string detailedDescriptionField;

        private string trailerField;

        private string formatField;

        private string idField;

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string Short_Title
        {
            get
            {
                return this.short_TitleField;
            }
            set
            {
                this.short_TitleField = value;
            }
        }

        /// <remarks/>
        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Genre", IsNullable = false)]
        public string[] Genres
        {
            get
            {
                return this.genresField;
            }
            set
            {
                this.genresField = value;
            }
        }

        /// <remarks/>
        public byte Runtime
        {
            get
            {
                return this.runtimeField;
            }
            set
            {
                this.runtimeField = value;
            }
        }

        /// <remarks/>
        public uint Start
        {
            get
            {
                return this.startField;
            }
            set
            {
                this.startField = value;
            }
        }

        /// <remarks/>
        public uint End
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        public string Language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        public string Cover
        {
            get
            {
                return this.coverField;
            }
            set
            {
                this.coverField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Actor", IsNullable = false)]
        public string[] Actors
        {
            get
            {
                return this.actorsField;
            }
            set
            {
                this.actorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Director", IsNullable = false)]
        public string[] Directors
        {
            get
            {
                return this.directorsField;
            }
            set
            {
                this.directorsField = value;
            }
        }

        /// <remarks/>
        public string Mediafile
        {
            get
            {
                return this.mediafileField;
            }
            set
            {
                this.mediafileField = value;
            }
        }

        /// <remarks/>
        public ushort Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string DetailedDescription
        {
            get
            {
                return this.detailedDescriptionField;
            }
            set
            {
                this.detailedDescriptionField = value;
            }
        }

        /// <remarks/>
        public string Trailer
        {
            get
            {
                return this.trailerField;
            }
            set
            {
                this.trailerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


}
