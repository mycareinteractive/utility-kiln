﻿using System;

namespace Culina.Vendors.Swank.Models
{
    public partial class Film
    {
        public string DirectorCSV
        {
            get
            {
                return String.Join(", ", directorsField);
            }
        }

        public string GenreCSV
        {
            get
            {
                return String.Join(", ", genresField);
            }
        }

        public string ActorCSV
        {
            get
            {
                return String.Join(", ", actorsField);
            }
        }
    }
}
