﻿using CsvHelper.Configuration;

namespace Culina.Vendors.Swank.Models
{
    public sealed class FilmMap : CsvClassMap<Film>
    {
        public FilmMap()
        {
            Map(m => m.Mediafile).Name("filename");
            Map(m => m.Title).Name("aceso.name");
            Map(m => m.Rating).Name("aceso.rating");
            Map(m => m.Runtime).Name("aceso.runtime");
            Map(m => m.Start).Name("aceso.activationDate");
            Map(m => m.End).Name("aceso.deactivationDate");
            Map(m => m.DetailedDescription).Name("description");
            Map(m => m.DirectorCSV).Name("aceso.directors");
            Map(m => m.ActorCSV).Name("aceso.actors");
            Map(m => m.GenreCSV).Name("aceso.genres");
        }
    }
}
