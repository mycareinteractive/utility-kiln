﻿using CsvHelper;
using Culina.Vendors.Swank.Models;
using System.Collections.Generic;
using System.IO;

namespace Culina.Vendors.Razuna.Controllers
{
    public class FromSwank
    {
        public List<Film> Films { get; set; }

        public List<Film> MakeFilmList(List<Film> swankFilms)
        {
            var films = new List<Film>();
            foreach (var f in swankFilms)
            {
                var rf = new Film();
                //rf.Format = f.Format;
                //rf.ID = f.ID;

                rf.Title = f.Title;

                //rf.Short_Title = f.Short_Title;
                rf.Rating = f.Rating;
                //rf.Language = f.Language;
                //rf.Cover = f.Cover;

                rf.Mediafile = f.Mediafile;
                rf.DetailedDescription = f.DetailedDescription;

                //rf.Trailer = f.Trailer;
                rf.Runtime = f.Runtime;
                rf.Start = f.Start;
                rf.End = f.End;
                //rf.Year = f.Year;
                rf.Genres = f.Genres;
                rf.Actors = f.Actors;
                rf.Directors = f.Directors;

                films.Add(rf);
            }

            return films;
        }

        public void ToCsv(List<Film> films, string outDir)
        {
            var tw = new StreamWriter(outDir + @"\razuna.csv");
            var csv = new CsvWriter(tw);
            csv.Configuration.RegisterClassMap<FilmMap>();
            csv.WriteRecords(films);
            tw.Close();
        }
    }
}
