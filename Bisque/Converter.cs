﻿using Culina.Vendors.SeaChange.Controllers;
using System;
using System.IO;
using System.Windows.Forms;
using SwankParser = Culina.Vendors.Swank.Controllers.Parser;

namespace Bisque
{
    public partial class Converter : Form
    {
        private string dirIn;
        private string dirOut;

        public Converter()
        {
            InitializeComponent();
        }

        private void DataScraper_Load(object sender, EventArgs e)
        {
            this.tbDirIn.Text = this.dirIn = @"G:\Clients\Aceso\projects\upscrape\data\in\_design";
            this.tbDirOut.Text = this.dirOut = @"G:\Clients\Aceso\projects\upscrape\data\out\_design";
        }

        private void GetFilesIn()
        {
            string[] files = Directory.GetFiles(this.dirIn, "*.html");
            foreach (string f in files)
            {
                tbStatus.AppendText(string.Format("Scraping {0}...\n", Path.GetFileName(f)));
                string ifile = File.ReadAllText(f);


                string outfile = this.dirOut + "\\" + Path.GetFileName(f);

            }

            tbStatus.AppendText("FINISHED writing files.\n");
        }

        private void btnDirIn_Click(object sender, EventArgs e)
        {
            fbdDirIn.SelectedPath = this.dirIn;
            DialogResult result = fbdDirIn.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.dirIn = fbdDirIn.SelectedPath;
                this.tbDirIn.Text = this.dirIn;
            }
        }

        private void btnDirOut_Click(object sender, EventArgs e)
        {
            fbdDirOut.SelectedPath = this.dirOut;
            DialogResult result = fbdDirOut.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.dirOut = fbdDirOut.SelectedPath;
                this.tbDirOut.Text = this.dirOut;
            }
        }

        private void btnScrape_Click(object sender, EventArgs e)
        {
            this.dirIn = this.tbDirIn.Text;
            this.dirOut = this.tbDirOut.Text;

            if (Directory.Exists(this.dirOut))
            {
                var result = MessageBox.Show("The output folder already exists, you might overwrite some files in the folder.\nDo you want to continue?", "", MessageBoxButtons.OKCancel);
                if (result != DialogResult.OK)
                    return;
            }
            else
            {
                Directory.CreateDirectory(this.dirOut);
            }

            var swank = new SwankParser(dirIn, "xml");
            var films = swank.ReadFiles();


            var scfw = new FromSwank();
            scfw.BatchConvert(films, dirOut);

            var rfs = new Culina.Vendors.Razuna.Controllers.FromSwank();
            var rfilms = rfs.MakeFilmList(films);
            rfs.ToCsv(rfilms, dirOut);

        }

        private void tbDirIn_DragEnter(object sender, DragEventArgs e)
        {
            DragDropEffects effect = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                if (Directory.Exists(path))
                    effect = DragDropEffects.Copy;
            }

            e.Effect = effect;
        }

        private void tbDirIn_DragDrop(object sender, DragEventArgs e)
        {
            var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            this.tbDirIn.Text = path;
            this.dirIn = path;
        }

        private void tbDirOut_DragEnter(object sender, DragEventArgs e)
        {
            DragDropEffects effect = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                if (Directory.Exists(path))
                    effect = DragDropEffects.Copy;
            }

            e.Effect = effect;
        }

        private void tbDirOut_DragDrop(object sender, DragEventArgs e)
        {
            var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            this.tbDirOut.Text = path;
            this.dirOut = path;
        }
    }
}
