﻿
namespace Bisque
{
    partial class Converter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnScrape = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.tbDirOut = new System.Windows.Forms.TextBox();
            this.tbDirIn = new System.Windows.Forms.TextBox();
            this.lblDirOut = new System.Windows.Forms.Label();
            this.btnDirOut = new System.Windows.Forms.Button();
            this.lblDirIn = new System.Windows.Forms.Label();
            this.btnDirIn = new System.Windows.Forms.Button();
            this.fbdDirOut = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdDirIn = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // btnScrape
            // 
            this.btnScrape.Location = new System.Drawing.Point(202, 89);
            this.btnScrape.Name = "btnScrape";
            this.btnScrape.Size = new System.Drawing.Size(75, 32);
            this.btnScrape.TabIndex = 26;
            this.btnScrape.Text = "Scrape";
            this.btnScrape.UseVisualStyleBackColor = true;
            this.btnScrape.Click += new System.EventHandler(this.btnScrape_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(22, 101);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 25;
            this.lblStatus.Text = "Status";
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(22, 126);
            this.tbStatus.Multiline = true;
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbStatus.Size = new System.Drawing.Size(257, 133);
            this.tbStatus.TabIndex = 24;
            // 
            // tbDirOut
            // 
            this.tbDirOut.AllowDrop = true;
            this.tbDirOut.Location = new System.Drawing.Point(54, 49);
            this.tbDirOut.Name = "tbDirOut";
            this.tbDirOut.Size = new System.Drawing.Size(144, 20);
            this.tbDirOut.TabIndex = 23;
            this.tbDirOut.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbDirOut_DragDrop);
            this.tbDirOut.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbDirOut_DragEnter);
            // 
            // tbDirIn
            // 
            this.tbDirIn.AllowDrop = true;
            this.tbDirIn.Location = new System.Drawing.Point(54, 19);
            this.tbDirIn.Name = "tbDirIn";
            this.tbDirIn.Size = new System.Drawing.Size(144, 20);
            this.tbDirIn.TabIndex = 22;
            this.tbDirIn.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbDirIn_DragDrop);
            this.tbDirIn.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbDirIn_DragEnter);
            // 
            // lblDirOut
            // 
            this.lblDirOut.AutoSize = true;
            this.lblDirOut.Location = new System.Drawing.Point(19, 53);
            this.lblDirOut.Name = "lblDirOut";
            this.lblDirOut.Size = new System.Drawing.Size(24, 13);
            this.lblDirOut.TabIndex = 21;
            this.lblDirOut.Text = "Out";
            // 
            // btnDirOut
            // 
            this.btnDirOut.Location = new System.Drawing.Point(204, 49);
            this.btnDirOut.Name = "btnDirOut";
            this.btnDirOut.Size = new System.Drawing.Size(75, 23);
            this.btnDirOut.TabIndex = 20;
            this.btnDirOut.Text = "Dir Out";
            this.btnDirOut.UseVisualStyleBackColor = true;
            this.btnDirOut.Click += new System.EventHandler(this.btnDirOut_Click);
            // 
            // lblDirIn
            // 
            this.lblDirIn.AutoSize = true;
            this.lblDirIn.Location = new System.Drawing.Point(19, 23);
            this.lblDirIn.Name = "lblDirIn";
            this.lblDirIn.Size = new System.Drawing.Size(16, 13);
            this.lblDirIn.TabIndex = 19;
            this.lblDirIn.Text = "In";
            // 
            // btnDirIn
            // 
            this.btnDirIn.Location = new System.Drawing.Point(204, 17);
            this.btnDirIn.Name = "btnDirIn";
            this.btnDirIn.Size = new System.Drawing.Size(75, 23);
            this.btnDirIn.TabIndex = 18;
            this.btnDirIn.Text = "Dir In";
            this.btnDirIn.UseVisualStyleBackColor = true;
            this.btnDirIn.Click += new System.EventHandler(this.btnDirIn_Click);
            // 
            // fbdDirIn
            // 
            this.fbdDirIn.ShowNewFolderButton = false;
            // 
            // Converter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 269);
            this.Controls.Add(this.btnScrape);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.tbDirOut);
            this.Controls.Add(this.tbDirIn);
            this.Controls.Add(this.lblDirOut);
            this.Controls.Add(this.btnDirOut);
            this.Controls.Add(this.lblDirIn);
            this.Controls.Add(this.btnDirIn);
            this.Name = "Converter";
            this.Text = "BisQue";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnScrape;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.TextBox tbDirOut;
        private System.Windows.Forms.TextBox tbDirIn;
        private System.Windows.Forms.Label lblDirOut;
        private System.Windows.Forms.Button btnDirOut;
        private System.Windows.Forms.Label lblDirIn;
        private System.Windows.Forms.Button btnDirIn;
        private System.Windows.Forms.FolderBrowserDialog fbdDirOut;
        private System.Windows.Forms.FolderBrowserDialog fbdDirIn;
    }
}

